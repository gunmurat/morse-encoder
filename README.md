# MorseEncoder

Morse Encoder API that is developed with Go Fiber.

## Run

```
go run main.go
```

## Test
```
go test ./test
```
