package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/morse-encoder/models"
	"gitlab.com/gunmurat/morse-encoder/service"
)

type MorseEncoderController interface {
	GetEncodedMorse(c *fiber.Ctx) error
}

type MorseEncoderControllerImpl struct {
	morseEncoderService service.MorseEncoderService
}

func NewMorseEncoderController(morseEncoderService service.MorseEncoderService) MorseEncoderController {
	return &MorseEncoderControllerImpl{
		morseEncoderService: morseEncoderService,
	}
}

func (m *MorseEncoderControllerImpl) GetEncodedMorse(c *fiber.Ctx) error {
	var morse models.MorseModel

	if err := c.BodyParser(&morse); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error":   err.Error(),
			"success": false,
		})
	}

	encodedMorse, errN := m.morseEncoderService.GetEncodedMorse(morse.Input)
	if errN != nil {
		return c.Status(400).JSON(fiber.Map{
			"error":   errN.Error(),
			"success": false,
		})
	}
	return c.Status(200).JSON(fiber.Map{
		"encoded_morse": encodedMorse,
		"success":       true,
	})
}
