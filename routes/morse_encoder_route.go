package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/morse-encoder/controllers"
	"gitlab.com/gunmurat/morse-encoder/service"
)

var morseController = controllers.NewMorseEncoderController(service.NewMorseEncoderService())

func MorseEncoderRoute(app *fiber.App) {
	app.Post("/morse", morseController.GetEncodedMorse)
}
