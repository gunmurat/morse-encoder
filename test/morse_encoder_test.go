package test

import (
	"testing"

	"gitlab.com/gunmurat/morse-encoder/service"
	"gotest.tools/assert"
)

func TestMorseEncoder(t *testing.T) {
	var s = service.NewMorseEncoderService()

	str := "Hello World"

	encodedMorse, err := s.GetEncodedMorse(str)

	assert.Equal(t, err, nil)
	assert.Equal(t, encodedMorse, ".... . .-.. .-.. --- / .-- --- .-. .-.. -..")
}
