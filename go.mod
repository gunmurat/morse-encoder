module gitlab.com/gunmurat/morse-encoder

go 1.18

require gotest.tools v2.2.0+incompatible

require (
	github.com/alwindoss/morse v1.0.1 // indirect
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/gofiber/fiber/v2 v2.29.0 // indirect
	github.com/google/go-cmp v0.5.7 // indirect
	github.com/klauspost/compress v1.15.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.34.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
)
