package service

import (
	"strings"

	"github.com/alwindoss/morse"
)

type MorseEncoderService interface {
	GetEncodedMorse(input string) (string, error)
}

type MorseEncoderServiceImpl struct {
}

func NewMorseEncoderService() MorseEncoderService {
	return &MorseEncoderServiceImpl{}
}

func (m *MorseEncoderServiceImpl) GetEncodedMorse(input string) (string, error) {
	h := morse.NewHacker()
	morseCode, err := h.Encode(strings.NewReader(input))
	if err != nil {
		return "", err
	}
	return string(morseCode), nil
}
